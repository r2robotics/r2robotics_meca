## Ball-bearing script
## 11.08.2016 by r-frank (BPLRFE/LearnFreeCAD on Youtube)
## based on ball bearing script by JMG
## (http://linuxforanengineer.blogspot.de/2013/08/free-cad-bearing-script.html)
#
#needed for inserting primitives
import Part
#needed for calculating the positions of the balls
import math
#needed for translation of torus
from FreeCAD import Base
#

#VALUES#####MODIFY ONLY THAT PART#####
#(inner radius of the o-ring)
InnerRadius=7.5
#(diameter of torus)
WidthDiameter=2.3
#####STOP MODIFYING#####

#Create new document
App.newDocument("Unnamed")
App.setActiveDocument("Unnamed")
App.ActiveDocument=App.getDocument("Unnamed")
Gui.ActiveDocument=Gui.getDocument("Unnamed")
#
#create torus
ORing=Part.makeTorus(InnerRadius+WidthDiameter/2.0, WidthDiameter/2.0)
Part.show(ORing)

#Make it pretty#
App.activeDocument().recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit") 
