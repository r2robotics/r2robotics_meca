## Cylindrical magnet script
## 08/07/2017 by Ksyy
#
#needed for inserting primitives
import Part
#needed for calculating the positions of the balls
import math
#needed for translation of torus
from FreeCAD import Base
#

#VALUES#####MODIFY ONLY THAT PART#####
#(radius of magnet)
R=5.0
#(length of magnet)
L=5.0
#(rounding radius for fillets)
RR=0.25
#####STOP MODIFYING#####

#Create new document
App.newDocument("Unnamed")
App.setActiveDocument("Unnamed")
App.ActiveDocument=App.getDocument("Unnamed")
Gui.ActiveDocument=Gui.getDocument("Unnamed")
#
#Inner Ring#
Magnet = Part.makeCylinder(R, L)
#get edges and apply fillets
Magnet = Magnet.makeFillet(RR, Magnet.Edges)
#create groove and show shape
Part.show(Magnet)

#Make it pretty#
App.activeDocument().recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit") 
