## Thrust-bearing script
## 06.07.2017 by Ksyy
## based on ball-bearing script by r-frank (BPLRFE/LearnFreeCAD on Youtube)
## based on ball bearing script by JMG
## (http://linuxforanengineer.blogspot.de/2013/08/free-cad-bearing-script.html)
#
#needed for inserting primitives
import Part
#needed for calculating the positions of the balls
import math
#needed for translation of torus
from FreeCAD import Base
#

#VALUES#####MODIFY ONLY THAT PART#####
#(inner radius of ring)
R1=30.0
#(outer radius of ring)
R2=42.5
#(thickness of the ring)
TH=1.5
#(number of rollers)
NRoller=40
#(length of roller)
LRoller=7.7
#(radius of roller)
RRoller=2.9/2.0
#(margin around roller)
MRoller=0.3
#(rounding radius for fillets)
RR=0.5
#####STOP MODIFYING#####

#first coordinate of center of ball
CRoller=((R2-R1)/2.0)+R1+LRoller/2.0
#second coordinate of center of ball
PRoller=TH/2.0
#
#Create new document
App.newDocument("Unnamed")
App.setActiveDocument("Unnamed")
App.ActiveDocument=App.getDocument("Unnamed")
Gui.ActiveDocument=Gui.getDocument("Unnamed")
#
#Inner Ring#
B1=Part.makeCylinder(R1,TH)
B2=Part.makeCylinder(R2,TH)
IR=B2.cut(B1)
##get edges, apply fillets and show shape
Bedges=IR.Edges
IRF=IR.makeFillet(RR,Bedges)
#Part.show(IRF)
##
##Rollers#
for i in range(NRoller):
  RollerCut=Part.makeCylinder(RRoller + MRoller, LRoller + MRoller * 2.0)
  Roller=Part.makeCylinder(RRoller, LRoller)
  Alpha=(i*2.0*math.pi)/NRoller
#  RV=(CRoller*math.cos(Alpha),CRoller*math.sin(Alpha),TH/2.0)
  RollerCut.rotate(App.Vector(0, 0, 0), App.Vector(1, 0, 0), 90)
  Roller.rotate(App.Vector(0, 0, 0), App.Vector(1, 0, 0), 90)

  TransCut=(0., CRoller+MRoller, TH/2.0)
  Trans=(0., CRoller, TH/2.0)
  RollerCut.translate(TransCut)
  Roller.translate(Trans)

  RollerCut.rotate(App.Vector(0, 0, 0), App.Vector(0, 0, 1), math.degrees(Alpha))
  Roller.rotate(App.Vector(0, 0, 0), App.Vector(0, 0, 1), math.degrees(Alpha))
  IRF=IRF.cut(RollerCut)
  Part.show(Roller)
Part.show(IRF)
##
#Make it pretty#
App.activeDocument().recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit") 

